package com.example.rutul.subwayorder;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class Drinks extends AppCompatActivity{

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drinks);

        addItemsOnCokeSpinner();
        addItemsOnZeviaSpinner();
        addItemsOnGingerSpinner();
        addItemsOnFantaSpinner();
    }

    private void addItemsOnCokeSpinner() {
        Spinner c_spinner = (Spinner) findViewById(R.id.spinner_coke);
        List<Integer> list = new ArrayList<>();
        for (int i =0;i<10;i++){
            list.add(i);
        }
        ArrayAdapter<Integer> dataAdapter1 = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,list);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        c_spinner.setAdapter(dataAdapter1);
    }

    private void addItemsOnZeviaSpinner() {
        Spinner d_spinner = (Spinner) findViewById(R.id.spinner_zevia);
        List<Integer> list = new ArrayList<>();
        for (int i =0;i<10;i++){
            list.add(i);
        }
        ArrayAdapter<Integer> dataAdapter2 = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,list);
        dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        d_spinner.setAdapter(dataAdapter2);
    }

    private void addItemsOnGingerSpinner() {
        Spinner g_spinner = (Spinner) findViewById(R.id.spinner_ginger);
        List<Integer> list = new ArrayList<>();
        for (int i =0;i<10;i++){
            list.add(i);
        }
        ArrayAdapter<Integer> dataAdapter3 = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,list);
        dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        g_spinner.setAdapter(dataAdapter3);
    }

    private void addItemsOnFantaSpinner() {
        Spinner f_spinner = (Spinner) findViewById(R.id.spinner_fanta);
        List<Integer> list = new ArrayList<>();
        for (int i =0;i<10;i++){
            list.add(i);
        }
        ArrayAdapter<Integer> dataAdapter4 = new ArrayAdapter<Integer>(this,android.R.layout.simple_spinner_item,list);
        dataAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        f_spinner.setAdapter(dataAdapter4);
    }
}
