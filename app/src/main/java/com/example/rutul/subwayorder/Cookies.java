package com.example.rutul.subwayorder;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * Created by Siddhi on 11/26/2017.
 */

public class Cookies extends AppCompatActivity {
    int cookieincrease = 0;
    int doublecookieincrease = 0;
    int total_price =0;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cookies);
    }
    public void increaseCookie(View view) {
        cookieincrease = cookieincrease + 1;
        displaycookie(cookieincrease);
        total_price=total_price+2;
        displaytotalcookie(total_price);
    }
    public void increasedoubleCookie(View view) {
        doublecookieincrease = doublecookieincrease + 1;
        displaydoublecookie(doublecookieincrease);
        total_price=total_price+4;
        displaytotalcookie(total_price);
    }
    public void clearCookie(View view) {
        cookieincrease =0;
        doublecookieincrease =0;
        total_price=0;
        displaycookie(cookieincrease);
        displaydoublecookie(doublecookieincrease);
        displaytotalcookie(total_price);
    }
    private void displaycookie(int numbercookie){
        TextView displayIntegercookie = (TextView) findViewById(R.id.QuantityCC);
        displayIntegercookie.setText("Quantity: " + numbercookie);
    }
    private void displaydoublecookie(int numberdoublecookie){
        TextView displayIntegerdoublecookie = (TextView) findViewById(R.id.QuantityDCC);
        displayIntegerdoublecookie.setText("Quantity: " + numberdoublecookie);

    }
    private void displaytotalcookie(int totalccokie){
        TextView displaytotalcookie = (TextView)findViewById(R.id.Total_Quantity);
        displaytotalcookie.setText("Total Amount: "+totalccokie);
    }
}
