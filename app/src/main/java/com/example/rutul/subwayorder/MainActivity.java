package com.example.rutul.subwayorder;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

    public ImageButton sandwich;
    public ImageButton drinks;
    public ImageButton chips;
    public ImageButton button4;

    private VideoView mvideoview;


    public void init() {
        sandwich = (ImageButton) findViewById(R.id.sandwich_button);
        sandwich.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent work = new Intent(MainActivity.this, Sandwich.class);

                startActivity(work);
            }
        });

    }

    public void init1() {
        drinks = (ImageButton) findViewById(R.id.drinks_button);
        drinks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent work = new Intent(MainActivity.this, com.example.rutul.subwayorder.Drinks.class);

                startActivity(work);
            }
        });
    }

    public void init2(){
        chips = (ImageButton) findViewById(R.id.chips_button);
        chips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent work = new Intent(MainActivity.this, Chips.class);

                startActivity(work);
            }
        });
    }

    public void init3(){
        chips = (ImageButton) findViewById(R.id.cookies_button);
        chips.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent work = new Intent(MainActivity.this, Cookies.class);

                startActivity(work);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        mvideoview.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mvideoview = (VideoView) findViewById(R.id.bgvideoView);

        Uri uri = Uri.parse("android resource://"+getPackageName()+"/"+R.raw.subway);

        mvideoview.setVideoURI(Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.subway));
        mvideoview.start();



        mvideoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
                mp.setVolume(0,0);
            }
        });

        init();
        init1();
        init2();
        init3();
    }
}
