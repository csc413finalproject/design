package com.example.rutul.subwayorder;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.rutul.subwayorder.R;

/**
 * Created by rutul on 11/15/2017.
 */

public class Chips extends AppCompatActivity{
    int dorinteger =0;
    int laysinteger=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chips);

    }

    public void increaseIntegerlays(View view) {
        laysinteger = laysinteger + 1;
        displaylays(laysinteger);
    }

    public void increaseIntegerdor(View view) {
        dorinteger = dorinteger + 1;
        displaydor(dorinteger);
    }
    public void decreaseIntegerlays(View view) {
        laysinteger = laysinteger - 1;
        if(laysinteger<0) laysinteger=0;
        displaylays(laysinteger);
    }
    public void decreaseIntegerdor(View view) {
        dorinteger = dorinteger - 1;
        if(dorinteger<0) dorinteger=0;
        displaydor(dorinteger);
    }
    private void displaydor(int numberdor){
        TextView displayIntegerdor = (TextView) findViewById(R.id.integernumdor);
        displayIntegerdor.setText("" + numberdor);

    }
    private void displaylays(int numberlays){
        TextView displayIntegerlays = (TextView) findViewById(R.id.integernumlays);
        displayIntegerlays.setText("" + numberlays);

    }
}
